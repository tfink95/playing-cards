// Enum and Struct Demo
// Tyler Fink

#include <iostream>
#include <conio.h>

using namespace std;

enum Suit
{
	SPADE, HEART, CLUB, DIAMOND
};

enum Rank
{
	TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};

struct Card
{
	Suit suit;
	Rank rank;
};

int main()
{

	_getch();
	return 0;
}



